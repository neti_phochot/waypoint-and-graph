﻿using core.grid;
using core.pathfinding;

namespace game.grid
{
    public class PathNodeGridMono : GridMono<PathNode>
    {
        protected override Grid<PathNode> GenerateGrid()
        {
            return new Grid<PathNode>(gridWidth, gridHeight, gridSize, (grid, x, y) => new PathNode(grid, x, y));
        }
    }
}