﻿using core.grid;
using UnityEngine;

namespace game.grid
{
    public abstract class GridMono<TGridObject> : MonoBehaviour
    {
        [Header("Grid")] public int gridWidth = 10;
        public int gridHeight = 10;
        public int gridSize = 1;
        private Grid<TGridObject> _grid;

        public Grid<TGridObject> Grid
        {
            get
            {
                if (_grid == null)
                {
                    _grid = GenerateGrid();
                }

                return _grid;
            }
        }

        protected abstract Grid<TGridObject> GenerateGrid();
    }
}