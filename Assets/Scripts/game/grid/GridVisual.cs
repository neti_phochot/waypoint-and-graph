﻿using core.grid;
using core.pathfinding;
using UnityEngine;

namespace game.grid
{
    public class GridVisual : MonoBehaviour
    {
        [SerializeField] private GameObject obstaclePrefab;
        [SerializeField] private PathNodeGridMono pathNodeGridMono;
        [SerializeField] private Color gridColor = Color.white;

        private Grid<PathNode> _grid;

        private void Awake()
        {
            _grid = pathNodeGridMono.Grid;
        }

        private void Start()
        {
            SpawnGridObstacle();
            AutoSizeCamera();
        }

        private void SpawnGridObstacle()
        {
            for (int row = 0; row < _grid.Width; row++)
            {
                for (int col = 0; col < _grid.Height; col++)
                {
                    PathNode pathNode = _grid.GetGridObject(row, col);
                    if (pathNode.isWalkable) continue;
                    GameObject obstacle = Instantiate(obstaclePrefab, transform);
                    obstacle.transform.position = pathNode.ToWorldPosition();
                }
            }
        }

        private void AutoSizeCamera()
        {
            Camera cam = Camera.main;
            cam.transform.position = new Vector3(_grid.Width / 2f, _grid.Width / 2f, _grid.Height / 2f);
            cam.orthographicSize = _grid.Height / 2f + 0.5f;
        }

        private void FixedUpdate()
        {
            for (int row = 0; row < _grid.Width; row++)
            {
                for (int col = 0; col < _grid.Height; col++)
                {
                    Vector3 startPoint = new Vector3(row * _grid.CellSize, 0, col * _grid.CellSize);

                    Vector3 endPoint1 = new Vector3(row + 1 * _grid.CellSize, 0, col * _grid.CellSize);
                    Debug.DrawLine(startPoint, endPoint1, gridColor);

                    Vector3 endPoint2 = new Vector3(row * _grid.CellSize, 0, col + 1 * _grid.CellSize);
                    Debug.DrawLine(startPoint, endPoint2, gridColor);
                }
            }

            Vector3 topRight = new Vector3(_grid.Width * _grid.CellSize, 0, _grid.Height * _grid.CellSize);

            Vector3 topLeft = new Vector3(0, 0, _grid.Height * _grid.CellSize);
            Debug.DrawLine(topLeft, topRight, gridColor);

            Vector3 belowLeft = new Vector3(_grid.Width * _grid.CellSize, 0, 0);
            Debug.DrawLine(belowLeft, topRight, gridColor);
        }
    }
}