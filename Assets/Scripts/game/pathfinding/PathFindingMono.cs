﻿using core.pathfinding;
using game.grid;
using UnityEngine;

namespace game.pathfinding
{
    public class PathFindingMono : MonoBehaviour
    {
        [SerializeField] private PathNodeGridMono pathNodeGridMono;

        private PathFinding _pathfinding;

        public PathFinding PathFinding
        {
            get
            {
                if (_pathfinding == null)
                {
                    _pathfinding = new PathFinding(pathNodeGridMono.Grid);
                }

                return _pathfinding;
            }
        }
    }
}