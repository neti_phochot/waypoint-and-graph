﻿using System.Collections.Generic;
using core.pathfinding;
using game.grid;
using UnityEngine;

namespace game.pathfinding
{
    public class PathFindingVisual : MonoBehaviour
    {
        [SerializeField] private Color pathColor = Color.white;
        [SerializeField] private PathFindingMono pathFindingMono;
        [SerializeField] private PathNodeGridMono pathNodeGridMono;

        private List<PathNode> _foundPaths;

        private void Awake()
        {
            _foundPaths = new List<PathNode>();
        }

        private void OnEnable()
        {
            pathFindingMono.PathFinding.OnPathFounded += OnPathFoundedEvent;
        }

        private void OnDisable()
        {
            pathFindingMono.PathFinding.OnPathFounded -= OnPathFoundedEvent;
        }

        private void OnPathFoundedEvent(List<PathNode> foundPaths)
        {
            _foundPaths = foundPaths;
        }

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;
            if (_foundPaths.Count == 0) return;
            var grid = pathNodeGridMono.Grid;
            for (int i = 0; i < _foundPaths.Count; i++)
            {
                if (i + 1 > _foundPaths.Count - 1) continue;
                PathNode nodeA = _foundPaths[i];
                PathNode nodeB = _foundPaths[i + 1];
                Gizmos.color = pathColor;
                float offsetY = 0.1f;
                Vector3 offsetPath = new Vector3(grid.CellSize, 0, grid.CellSize) * 0.5f;
                Gizmos.DrawLine(new Vector3(nodeA.x * grid.CellSize, offsetY, nodeA.y * grid.CellSize) + offsetPath,
                    new Vector3(nodeB.x * grid.CellSize, offsetY, nodeB.y * grid.CellSize) + offsetPath);
            }
        }
    }
}