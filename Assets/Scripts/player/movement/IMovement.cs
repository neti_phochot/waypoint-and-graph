﻿using UnityEngine;

namespace player.movement
{
    public interface IMovement
    {
        void MoveTo(Vector3 position);
    }
}