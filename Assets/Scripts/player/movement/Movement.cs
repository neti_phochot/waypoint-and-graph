﻿using UnityEngine;

namespace player.movement
{
    public abstract class Movement : MonoBehaviour, IMovement, ILocation
    {
        public abstract void MoveTo(Vector3 position);
        public virtual Vector3 GetLocation() => transform.position;
    }
}