﻿using UnityEngine;

namespace player.movement.playermovement
{
    public class BasicPlayerMovement : PlayerMovement
    {
        public override void MoveTo(Vector3 position)
        {
            Quaternion lookTarget = Quaternion.LookRotation(position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookTarget, turnSpeed * Time.deltaTime);
            transform.Translate(0, 0, moveSpeed * Time.deltaTime);
        }
    }
}