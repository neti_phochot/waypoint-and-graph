﻿using UnityEngine;

namespace player.movement.playermovement
{
    [RequireComponent(typeof(Player))]
    public abstract class PlayerMovement : Movement
    {
        [Header("Player")] public float moveSpeed = 5f;
        public float turnSpeed = 5f;

        protected Player Player { get; private set; }

        protected virtual void Awake()
        {
            Player = GetComponent<Player>();
        }
    }
}