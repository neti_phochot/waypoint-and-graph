﻿using System;
using UnityEngine;

namespace player.movement.playermovement
{
    public class BetterPlayerMovement : PlayerMovement
    {
        [Range(1, 2)] [Header("Tracker")] [SerializeField]
        float trackerMoveSpeedMultiplier = 1f;

        [SerializeField] float trackerRange = 10f;

        [Header("Tracker Debug")] [SerializeField]
        private Color trackerLineColor = Color.white;

        [SerializeField] private Color trackerColor = Color.white;
        [SerializeField] private float trackerSize = 0.5f;

        private TrackerMovement _trackerMovement;

        protected override void Awake()
        {
            base.Awake();

            InitPlayerTracker();
        }

        public override Vector3 GetLocation()
        {
            return _trackerMovement.GetLocation();
        }

        private void InitPlayerTracker()
        {
            _trackerMovement = new GameObject($"{Player.name}'s tracker").AddComponent<TrackerMovement>();

            //Set tracker translate and rotation same as player
            _trackerMovement.transform.position = Player.transform.position;
            _trackerMovement.transform.rotation = Player.transform.rotation;
        }

        private bool IsTrackerInPlayerRange()
        {
            return Vector3.Distance(Player.transform.position, _trackerMovement.transform.position) < trackerRange;
        }

        public override void MoveTo(Vector3 position)
        {
            //Check if tracker is not move further player range 
            if (IsTrackerInPlayerRange())
            {
                //Update tracker move speed
                _trackerMovement.moveSpeed = moveSpeed * trackerMoveSpeedMultiplier;

                //Move tracker to target
                _trackerMovement.MoveTo(position);
            }

            //Make player follow tracker

            Quaternion lookTarget = Quaternion.LookRotation(_trackerMovement.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookTarget, turnSpeed * Time.deltaTime);
            transform.Translate(0, 0, moveSpeed * Time.deltaTime);
        }

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;

            //Line from player to tracker
            Gizmos.color = trackerLineColor;
            var trackerPosition = _trackerMovement.transform.position;
            Gizmos.DrawLine(Player.transform.position, trackerPosition);

            //Tracker
            Gizmos.color = trackerColor;
            Gizmos.DrawSphere(trackerPosition, trackerSize);
        }
    }
}