﻿using UnityEngine;

namespace player.movement
{
    public interface ILocation
    {
        Vector3 GetLocation();
    }
}