﻿using UnityEngine;

namespace player.movement
{
    public class TrackerMovement : Movement
    {
        public float moveSpeed;

        public override void MoveTo(Vector3 position)
        {
            Quaternion lookTarget = Quaternion.LookRotation(position - transform.position);
            transform.rotation = lookTarget;
            transform.Translate(0, 0, moveSpeed * Time.deltaTime);
        }
    }
}