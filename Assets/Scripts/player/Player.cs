﻿using player.movement;
using player.movement.playermovement;
using UnityEngine;

namespace player
{
    public class Player : MonoBehaviour, IPlayerAI, IMovement, ILocation
    {
        private PlayerAI _playerAI;
        private PlayerMovement _movement;

        private void Awake()
        {
            _movement = GetComponent<PlayerMovement>();
        }

        #region PlayerMovement

        public void MoveTo(Vector3 position)
        {
            _movement.MoveTo(position);
        }

        public Vector3 GetLocation()
        {
            return _movement.GetLocation();
        }

        #endregion

        #region PlayerAI

        public void SetAI(PlayerAI playerAI)
        {
            _playerAI = playerAI;
        }

        public PlayerAI GetAI()
        {
            return _playerAI;
        }

        public bool HasAI()
        {
            return _playerAI;
        }

        #endregion
    }
}