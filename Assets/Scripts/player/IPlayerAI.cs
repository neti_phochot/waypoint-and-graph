﻿namespace player
{
    public interface IPlayerAI
    {
        void SetAI(PlayerAI playerAI);
        PlayerAI GetAI();
        bool HasAI();
    }
}