﻿using UnityEngine;

namespace player.ai
{
    public abstract class State
    {
        protected enum StateType
        {
            ENTER,
            UPDATE,
            EXIT
        }

        private StateType _stateType;
        private State _nextState;

        protected State()
        {
            _stateType = StateType.ENTER;
        }

        protected virtual void Enter()
        {
            Debug.Log("STATE ENTER!");
            _stateType = StateType.UPDATE;
        }

        protected virtual void Update()
        {
            _stateType = StateType.UPDATE;
        }

        public virtual void Exit()
        {
            Debug.Log("STATE EXIT!");
        }

        public void NextState(State state)
        {
            _nextState = state;
            _stateType = StateType.EXIT;
        }

        public State Process()
        {
            if (_stateType == StateType.UPDATE) Update();
            else if (_stateType == StateType.ENTER) Enter();
            else if (_stateType == StateType.EXIT)
            {
                return _nextState;
            }

            return this;
        }
    }
}