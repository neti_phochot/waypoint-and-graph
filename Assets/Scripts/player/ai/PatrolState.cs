﻿using System.Collections.Generic;
using core.pathfinding;
using UnityEngine;
using waypoint;

namespace player.ai
{
    public class PatrolState : State
    {
        private readonly List<PathNode> _pathNodes;
        private readonly Player _player;

        private int _currentPathIndex;

        private Vector3 _goalLocation;

        public PatrolState(Player player, Waypoint waypoint)
        {
            _player = player;
            _pathNodes = _player.GetAI().GetAStarPaths(waypoint);
        }

        protected override void Enter()
        {
            base.Enter();

            //No path found
            if (_pathNodes.Count == 0)
            {
                Debug.LogWarning("AI path not found, Select random waypoint!");
                _player.GetAI().SetDestination(_player.GetAI().GetNextWaypoint());
                return;
            }

            UpdateGoalLocation();
        }

        protected override void Update()
        {
            base.Update();

            if (Vector3.Distance(_player.GetLocation(), _goalLocation) < 0.5f)
            {
                if (_currentPathIndex >= _pathNodes.Count)
                {
                    //Reaches goal location
                    _player.GetAI().SetDestination(_player.GetAI().GetNextWaypoint());
                    return;
                }

                //Set new node location
                UpdateGoalLocation();
                return;
            }

            //Move to node location
            _player.MoveTo(_goalLocation);
        }

        private void UpdateGoalLocation()
        {
            PathNode path = _pathNodes[_currentPathIndex++];
            _goalLocation = path.ToWorldPosition();
        }
    }
}