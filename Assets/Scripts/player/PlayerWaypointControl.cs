﻿using System;
using UnityEngine;
using waypoint;

namespace player
{
    public class PlayerWaypointControl : MonoBehaviour
    {
        [SerializeField] private WaypointManager waypointManager;
        [SerializeField] private PlayerAI playerAI;

        [Header("GUI")] [SerializeField] private string guiName;
        [SerializeField] private float buttonWidth = 20;

        [SerializeField] private float buttonHeight = 10;
        [SerializeField] private float guiPadding = 5;

        [Header("GUI Auto Size")] [SerializeField]
        private bool autoButtonSize;

        [Range(0, 1f)] [SerializeField] private float flexibleHeightFactor;
        [Header("Key bind")] [SerializeField] private bool showGUI;

        private void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                showGUI = !showGUI;
            }
        }

        private void OnGUI()
        {
            if (!showGUI) return;

            GUI.Box(new Rect(guiPadding, guiPadding, 200, 50), guiName);

            int waypointsCount = waypointManager.Waypoints.Count;

            if (autoButtonSize)
            {
                buttonHeight = (Screen.height - guiPadding * waypointsCount) / waypointsCount;
                buttonWidth = Screen.width * flexibleHeightFactor;
            }

            float padding = 0;
            for (int i = 0; i < waypointsCount; i++)
            {
                Waypoint waypoint = waypointManager.Waypoints[i];
                if (GUI.Button(new Rect(Screen.width - buttonWidth - guiPadding, buttonHeight * i + padding,
                        buttonWidth, buttonHeight), $"{waypoint.name}"))
                {
                    playerAI.SetDestination(waypoint);
                }

                padding += guiPadding;
            }
        }
    }
}