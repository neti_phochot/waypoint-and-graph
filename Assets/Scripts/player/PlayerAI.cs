﻿using System;
using System.Collections.Generic;
using core.pathfinding;
using game.pathfinding;
using player.ai;
using UnityEngine;
using waypoint;
using Random = UnityEngine.Random;

namespace player
{
    [RequireComponent(typeof(Player))]
    public class PlayerAI : MonoBehaviour
    {
        public enum PlayerState
        {
            IDLE,
            PATROL
        }

        [SerializeField] private WaypointManager waypointManager;
        [SerializeField] private PathFindingMono pathFindingMono;

        [Header("AI")] [SerializeField] private PlayerState initState;

        private Player _player;
        private State _playerBehaviour;
        private Waypoint _currentWaypoint;

        private void Awake()
        {
            _player = GetComponent<Player>();
            _player.SetAI(this);
            _playerBehaviour = new IdleState();
            _currentWaypoint = GetNextWaypoint();
        }

        private void Start()
        {
            SetPlayerState(initState);
        }

        private void Update()
        {
            _playerBehaviour = _playerBehaviour.Process();
        }

        public void SetPlayerState(PlayerState playerState)
        {
            switch (playerState)
            {
                case PlayerState.IDLE:
                    _playerBehaviour.NextState(new IdleState());
                    break;
                case PlayerState.PATROL:
                    _playerBehaviour.NextState(new PatrolState(_player, _currentWaypoint));
                    break;
            }
        }

        public List<PathNode> GetAStarPaths(Waypoint goalWaypoint)
        {
            return pathFindingMono.PathFinding.FindPath(_player.GetLocation(), goalWaypoint.GetLocation());
        }

        public void SetDestination(Waypoint waypoint)
        {
            _currentWaypoint = waypoint;
            SetPlayerState(PlayerState.PATROL);
        }

        public Waypoint GetNextWaypoint()
        {
            Waypoint nextWaypoint;
            if (_currentWaypoint != null && _currentWaypoint.HasConnection())
            {
                //Get linked Waypoint
                Debug.Log("Linked waypoint");
                nextWaypoint = _currentWaypoint.ConnectTo;
            }
            else
            {
                //TODO: Fix bug - array out of range
                //Get random Waypoint
                int randIndex = Random.Range(0, waypointManager.Waypoints.Count);
                Debug.Log($"Random waypoint index: {randIndex}");
                nextWaypoint = waypointManager.Waypoints[randIndex];
            }

            return nextWaypoint;
        }
    }
}