﻿using System.Collections.Generic;
using UnityEngine;

namespace waypoint
{
    public class WaypointManager : MonoBehaviour
    {
        [SerializeField] private Transform waypointContainer;

        private List<Waypoint> _waypoints;

        public List<Waypoint> Waypoints
        {
            get
            {
                if (_waypoints == null)
                {
                    _waypoints = new List<Waypoint>();
                    for (int i = 0; i < waypointContainer.childCount; i++)
                    {
                        var obj = waypointContainer.GetChild(i);
                        if (obj.TryGetComponent(out Waypoint waypoint))
                        {
                            _waypoints.Add(waypoint);
                        }
                    }
                }

                return _waypoints;
            }
        }
    }
}