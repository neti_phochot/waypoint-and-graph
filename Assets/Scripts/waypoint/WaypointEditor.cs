﻿using UnityEngine;

namespace waypoint
{
    public class WaypointEditor : MonoBehaviour
    {
        [SerializeField] private Waypoint waypoint;

        private Camera _cam;

        private void Awake()
        {
            _cam = Camera.main;
        }


        private void OnMouseDrag()
        {
            if (Input.GetMouseButtonDown(0)) return;
            //Source: https://answers.unity.com/questions/12322/drag-gameobject-with-mouse.html
            float distanceToScreen = _cam.WorldToScreenPoint(waypoint.transform.position).z;
            Vector3 posMove = _cam.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceToScreen));

            var waypointTransform = waypoint.transform;
            waypointTransform.position = new Vector3(posMove.x, waypointTransform.position.y, posMove.z);
        }
    }
}