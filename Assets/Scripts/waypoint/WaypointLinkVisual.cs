﻿using UnityEngine;

namespace waypoint
{
    public class WaypointLinkVisual : MonoBehaviour
    {
        [SerializeField] private WaypointManager waypointManager;

        [Header("Waypoint")] [SerializeField] private float waypointRadius = 0.25f;
        [SerializeField] private Color waypointColor = Color.green;

        [Header("Waypoint Link")] [SerializeField]
        private Color waypointLinkColor = Color.cyan;

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;

            foreach (Waypoint waypoint in waypointManager.Waypoints)
            {
                Gizmos.color = waypointColor;
                Gizmos.DrawSphere(waypoint.GetLocation(), waypointRadius);

                if (!waypoint.HasConnection()) continue;
                Gizmos.color = waypointLinkColor;
                Gizmos.DrawLine(waypoint.GetLocation(), waypoint.ConnectTo.GetLocation());
            }
        }
    }
}