﻿using player.movement;
using UnityEngine;

namespace waypoint
{
    public class Waypoint : MonoBehaviour, ILocation
    {
        [SerializeField] private Waypoint connectTo;
        public Waypoint ConnectTo => connectTo;
        public bool HasConnection() => connectTo;
        public Vector3 GetLocation() => transform.position;
    }
}