﻿using System;
using UnityEngine;

namespace core.grid
{
    public class Grid<TGridObject>
    {
        public int Width { get; }
        public int Height { get; }
        public int CellSize { get; }
        public TGridObject[,] GridObjects { get; }

        public Grid(int width, int height, int cellSize,
            Func<Grid<TGridObject>, int, int, TGridObject> createGridObject)
        {
            Width = width;
            Height = height;
            CellSize = cellSize;

            GridObjects = new TGridObject[width, height];
            for (int row = 0; row < GridObjects.GetLength(0); row++)
            {
                for (int cal = 0; cal < GridObjects.GetLength(1); cal++)
                {
                    GridObjects[row, cal] = createGridObject(this, row, cal);
                }
            }
        }

        public TGridObject GetGridObject(int row, int cal)
        {
            if (row >= 0 && cal >= 0 && row < Width && cal < Height)
            {
                return GridObjects[row, cal];
            }

            return default;
        }

        public TGridObject GetGridObjectFromWorldPosition(Vector3 worldPosition)
        {
            int x = Mathf.FloorToInt(worldPosition.x / CellSize);
            int y = Mathf.FloorToInt(worldPosition.z / CellSize);
            return GetGridObject(x, y);
        }
    }
}