﻿using System;
using System.Collections.Generic;
using core.grid;
using UnityEngine;
using Random = UnityEngine.Random;

namespace core.pathfinding
{
    public class PathFinding
    {
        private const float STRAIGHT_LINE_DISTANCE = 10f;
        private const float DIAGONAL_LINE_DISTANCE = 14f; //sqrt(2)

        public event Action<List<PathNode>> OnPathFounded;

        private readonly Grid<PathNode> _grid;

        private List<PathNode> _openList;
        private List<PathNode> _closeList;

        public PathFinding(Grid<PathNode> grid)
        {
            _grid = grid;
            
            AddObstacle();
        }

        private void AddObstacle()
        {
            foreach (var pathNode in _grid.GridObjects)
            {
                //Test Obstacle
                int chanceOfObstacle = 10;
                pathNode.isWalkable = Random.Range(0, 100) > chanceOfObstacle;
            }
        }

        public List<PathNode> FindPath(Vector3 startPosition, Vector3 goalPosition)
        {
            PathNode startNode = _grid.GetGridObjectFromWorldPosition(startPosition);
            PathNode goalNode = _grid.GetGridObjectFromWorldPosition(goalPosition);

            //The set of nodes to be evaluated
            _openList = new List<PathNode>();

            //The set of nodes already evaluated
            _closeList = new List<PathNode>();

            foreach (var pathNode in _grid.GridObjects)
            {
                pathNode.gCost = float.MaxValue;
                pathNode.hCost = 0;
                pathNode.fCost = 0;
                pathNode.CameFromNode = null;
                pathNode.CalculateFCost();
            }

            startNode.gCost = 0;
            startNode.CalculateFCost();

            //Add the start node to OPEN
            _openList.Add(startNode);

            //Loop
            while (_openList.Count > 0)
            {
                //Find node in OPEN with the lowest f_cost
                PathNode currentNode = FindLowestFCostNode(_openList);

                //Remove current from OPEN
                _openList.Remove(currentNode);

                //Add current to CLOSED
                _closeList.Add(currentNode);

                if (currentNode == goalNode)
                {
                    //Path has been found
                    List<PathNode> foundPath = GetPaths(currentNode);
                    OnPathFounded?.Invoke(foundPath);
                    return foundPath;
                }

                //Loop neighbor nodes
                foreach (PathNode neighbourNode in FindNeighbourPathNodes(currentNode))
                {
                    //Skip if neighbour node is in CLOSED
                    if (_closeList.Contains(neighbourNode) || !neighbourNode.isWalkable) continue;

                    //Calculate tentative g_cost
                    float tentativeGCost = currentNode.gCost + Heuristic(currentNode, neighbourNode);

                    //Check if new g_cost is closer the old g_cost
                    if (tentativeGCost < neighbourNode.gCost)
                    {
                        //Set f_cost of neighbour
                        neighbourNode.gCost = tentativeGCost;
                        neighbourNode.hCost = Heuristic(neighbourNode, goalNode);
                        neighbourNode.CalculateFCost();

                        //Set parent of neighbour
                        neighbourNode.CameFromNode = currentNode;
                    }

                    //Check if neighbour node not in OPEN then add to OPEN
                    if (!_openList.Contains(neighbourNode))
                    {
                        _openList.Add(neighbourNode);
                    }
                }
            }

            return new List<PathNode>();
        }

        private PathNode FindLowestFCostNode(List<PathNode> nodeList)
        {
            PathNode lowestFCostNode = nodeList[0];
            foreach (var node in nodeList)
            {
                if (node.fCost < lowestFCostNode.fCost)
                {
                    lowestFCostNode = node;
                }
            }

            return lowestFCostNode;
        }

        private float Heuristic(PathNode startNode, PathNode goalNode)
        {
            //Source: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#diagonal-distance
            //Diagonal Distance Calculate
            float dx = Mathf.Abs(startNode.x - goalNode.x);
            float dy = Mathf.Abs(startNode.y - goalNode.y);
            return STRAIGHT_LINE_DISTANCE * Mathf.Max(dx, dy) +
                   (DIAGONAL_LINE_DISTANCE - STRAIGHT_LINE_DISTANCE) * Mathf.Min(dx, dy);
        }


        private List<PathNode> GetPaths(PathNode endNode)
        {
            List<PathNode> paths = new List<PathNode> { endNode };
            PathNode currentPath = endNode;
            while (currentPath.CameFromNode != null)
            {
                paths.Add(currentPath.CameFromNode);
                currentPath = currentPath.CameFromNode;
            }

            paths.Reverse();
            return paths;
        }

        private IEnumerable<PathNode> FindNeighbourPathNodes(PathNode currentNode)
        {
            //8 directions
            List<PathNode> neighbourNodes = new List<PathNode>();

            if (currentNode.x - 1 >= 0)
            {
                //LEFT
                neighbourNodes.Add(_grid.GetGridObject(currentNode.x - 1, currentNode.y));

                if (currentNode.y + 1 < _grid.Height)
                {
                    //TOP LEFT
                    neighbourNodes.Add(_grid.GetGridObject(currentNode.x - 1, currentNode.y + 1));
                }

                if (currentNode.y - 1 >= 0)
                {
                    //BOTTOM LEFT
                    neighbourNodes.Add(_grid.GetGridObject(currentNode.x - 1, currentNode.y - 1));
                }
            }

            if (currentNode.x + 1 < _grid.Width)
            {
                //RIGHT
                neighbourNodes.Add(_grid.GetGridObject(currentNode.x + 1, currentNode.y));

                if (currentNode.y + 1 < _grid.Height)
                {
                    //TOP RIGHT
                    neighbourNodes.Add(_grid.GetGridObject(currentNode.x + 1, currentNode.y + 1));
                }

                if (currentNode.y - 1 >= 0)
                {
                    //BOTTOM RIGHT
                    neighbourNodes.Add(_grid.GetGridObject(currentNode.x + 1, currentNode.y - 1));
                }
            }

            if (currentNode.y + 1 < _grid.Height)
            {
                //TOP CENTER
                neighbourNodes.Add(_grid.GetGridObject(currentNode.x, currentNode.y + 1));
            }

            if (currentNode.y - 1 >= 0)
            {
                //BOTTOM CENTER
                neighbourNodes.Add(_grid.GetGridObject(currentNode.x, currentNode.y - 1));
            }

            return neighbourNodes;
        }
    }
}