﻿using core.grid;
using UnityEngine;

namespace core.pathfinding
{
    public class PathNode
    {
        public Grid<PathNode> Grid { get; }
        public PathNode CameFromNode;

        public int x;
        public int y;

        public float gCost;
        public float hCost;
        public float fCost;

        public bool isWalkable = true;

        public PathNode(Grid<PathNode> grid, int x, int y)
        {
            Grid = grid;
            this.x = x;
            this.y = y;
        }

        public void CalculateFCost()
        {
            fCost = gCost + hCost;
        }

        public Vector3 ToWorldPosition()
        {
            return new Vector3(x, 0, y) + new Vector3(Grid.CellSize, 0, Grid.CellSize) * 0.5f;
        }

        public override string ToString()
        {
            return $"[{x}, {y}]";
        }
    }
}